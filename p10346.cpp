#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    int a,n,k;
    while(scanf("%d%d", &n, &k)!=EOF && k>1){
        a=n;
        while(n>=k){
            a=a+(n/k);
            n=(n/k)+(n%k);
        }
        printf("%d\n",a);
    }
    return 0;
}
